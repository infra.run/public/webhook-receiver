package main

import (
	"crypto/subtle"
	"fmt"
	"io/ioutil"
	"net/http"
	"os/exec"
	"sync"

	"github.com/labstack/echo/v4"
	"github.com/yookoala/realpath"
	"gopkg.in/yaml.v2"
)

type webhook struct {
	Handler string
	Repo    string
	Header  string
	Secret  string
}

type configuration struct {
	Listen   string
	Webhooks []webhook
}

func readConfig() *configuration {
	raw, err := ioutil.ReadFile("config.yml")
	if err != nil {
		panic(err)
	}

	var conf configuration
	if err := yaml.Unmarshal(raw, &conf); err != nil {
		panic(err)
	}

	return &conf
}

type repository struct {
	mut  *sync.Mutex
	path string
}

func newrepository(path string) *repository {
	return &repository{
		mut:  new(sync.Mutex),
		path: path,
	}
}

func (r *repository) run(command string, args ...string) error {
	cmd := exec.Command(command, args...)
	cmd.Dir = r.path
	return cmd.Run()
}

// Pull from remote, including submodules
func (r *repository) Pull() error {
	r.mut.Lock()
	defer r.mut.Unlock()
	if err := r.run("git", "pull", "--ff-only"); err != nil {
		return err
	}
	if err := r.run("git", "submodule", "init"); err != nil {
		return err
	}
	if err := r.run("git", "submodule", "update"); err != nil {
		return err
	}
	return nil
}

func main() {
	e := echo.New()
	conf := readConfig()
	seenRepos := make(map[string]string, len(conf.Webhooks)) // realpath -> given path
	seenHandlers := make(map[string]struct{}, len(conf.Webhooks))
	for _, hook := range conf.Webhooks {
		// make sure no repo is configured twice
		path, err := realpath.Realpath(hook.Repo)
		if err != nil {
			panic(err)
		}
		if other, seen := seenRepos[path]; seen {
			panic(fmt.Sprintf("duplicate repository configured: '%s' and '%s' are the same", other, hook.Repo))
		}
		seenRepos[path] = hook.Repo

		// make sure no endpoint is configured twice
		if _, seen := seenHandlers[hook.Handler]; seen {
			panic(fmt.Sprintf("duplicate handler configured: '%s'", hook.Handler))
		}
		seenHandlers[hook.Handler] = struct{}{}

		repo := newrepository(hook.Repo)
		secret := []byte(hook.Secret)

		e.POST(hook.Handler, func(c echo.Context) error {
			token := []byte(c.Request().Header.Get(hook.Header))
			if subtle.ConstantTimeCompare(token, secret) == 0 {
				return c.String(http.StatusUnauthorized, "NOPE\n")
			}

			go repo.Pull()
			return c.String(http.StatusOK, "OK\n")
		})
	}
	e.Logger.Fatal(e.Start(conf.Listen))
}

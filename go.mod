module gitlab.com/infra.run/public/webhook-receiver

go 1.14

require (
	github.com/go-git/go-git/v5 v5.1.0
	github.com/labstack/echo/v4 v4.1.16
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/valyala/fasttemplate v1.1.1 // indirect
	github.com/yookoala/realpath v1.0.0
	golang.org/x/net v0.0.0-20200625001655-4c5254603344 // indirect
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/yaml.v2 v2.3.0
)

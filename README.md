# webhook-receiver

Receive webhooks from e.g. Gitlab and pull git repositories.

## Quickstart

``` bash
git clone git@gitlab.com:infra.run/infrastructure/blobstorage.git /tmp/blobstorage
cp example.config.yml config.yml
go build ./cmd/webhook-receiver && ./webhook-receiver &
cd /tmp/blobstorage; git reset --hard f155fb2; cd -
curl -X POST -H "X-Gitlab-Token: Shaish5reipha5ru" http://localhost:8080/blobstorage
ls -l /tmp/blobstorage
```
